# uApp Explorer Scope #

Ubuntu Touch scope for uApp Explorer

## Resources ##

- [Docs for go-unityscopes](https://godoc.org/launchpad.net/go-unityscopes/v2)
- [Go Scope Tutorial](https://developer.ubuntu.com/en/phone/scopes/tutorials/developing-scopes-go/)
