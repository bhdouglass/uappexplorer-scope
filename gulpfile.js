var gulp = require('gulp');
var shell = require('gulp-shell');
var template = require('gulp-template');
var del = require('del');
var minimist = require('minimist');
var fs = require('fs');
var path = require('path');

var sdk = 'ubuntu-sdk-15.04';
var paths = {
    src: {
        click: ['click/manifest.json', 'click/uappexplorer-scope.apparmor'],
        scope: ['images/icon.png', 'images/logo.png', 'src/uappexplorer-scope.bhdouglass_uappexplorer-scope.ini'],
        go_template: 'src/uappexplorer-scope.go',
        go: 'dist/uappexplorer-scope/uappexplorer-scope.go',
    },
    dist: {
        click: 'dist',
        scope: 'dist/uappexplorer-scope/',
        go: 'dist/uappexplorer-scope/uappexplorer-scope.bhdouglass_uappexplorer-scope',
    }
};

var config = minimist(process.argv.slice(2), {
    default: {
        api: 'http://uappexplorer.com',
    }
});

gulp.task('clean', function() {
    del.sync(paths.dist.click);
});

gulp.task('move-click', function() {
    return gulp.src(paths.src.click)
        .pipe(gulp.dest(paths.dist.click));
});

gulp.task('move-scope', function() {
    return gulp.src(paths.src.scope)
        .pipe(gulp.dest(paths.dist.scope));
});

gulp.task('move-go', function() {
    return gulp.src(paths.src.go_template)
    .pipe(template({
        api: config.api
    }))
    .pipe(gulp.dest(paths.dist.scope));
});

gulp.task('build-go', ['clean', 'move-click', 'move-scope', 'move-go'], shell.task('GOPATH=`pwd`/go go build -o ' + paths.dist.go + ' ' + paths.src.go));

gulp.task('build-go-armhf', ['clean', 'move-click', 'move-scope', 'move-go'], shell.task(
    'CGO_ENABLED=1 ' +
    'GOPATH=`pwd`/go ' +
    'GOARCH=arm ' +
    'GOARM=7 ' +
    'CXX=arm-linux-gnueabihf-g++ ' +
    'PKG_CONFIG_LIBDIR=/usr/lib/arm-linux-gnueabihf/pkgconfig:/usr/lib/pkgconfig:/usr/share/pkgconfig ' +
    'CC=arm-linux-gnueabihf-gcc ' +
    'go build -o ' + paths.dist.go + ' ' +
    '-ldflags \'-extld=arm-linux-gnueabihf-g++\' ' + paths.src.go
));

gulp.task('run', ['build-go'], shell.task(
    'unity-scope-tool ' + paths.dist.scope + '/uappexplorer-scope.bhdouglass_uappexplorer-scope.ini'
));

gulp.task('default', ['run']);

gulp.task('build-chroot', shell.task('click chroot -a armhf -f ' + sdk + ' run gulp build-go-armhf'));

gulp.task('build-click', ['build-chroot'], shell.task('click build dist'));

//TODO call this after the click has been built so we can actually find it
function findClick() {
    var click = null;

    var dir = fs.readdirSync('.');
    dir.forEach(function(file) {
        if (path.extname(file) == '.click') {
            click = file;
        }
    });

    return click;
}

gulp.task('push-click', ['build-click'], shell.task(
    'adb push ./' + findClick() + ' /home/phablet/')
);

gulp.task('install-click', ['push-click'], shell.task(
    'echo "pkcon install-local --allow-untrusted ./' + findClick() + '" | phablet-shell'
));
